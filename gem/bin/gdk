#!/usr/bin/env ruby

# frozen_string_literal: true

require 'yaml'

$LOAD_PATH.unshift(File.expand_path('../lib', __dir__))
require 'gitlab_development_kit'

# Gitlab Development Kit CLI launcher
#
# Note to contributors: this script must not change (much) because it is
# installed outside the gitlab-development-kit repository with 'gem
# install'. Edit lib/gdk.rb to define new commands.
module GDK
  ROOT_CHECK_FILE = '.gdk-install-root'
  DEFAULT_INIT_DIRECTORY = File.join(Dir.pwd, 'gitlab-development-kit')

  def self.launcher_main
    case ARGV.first
    when 'init'
      if ARGV.count > 2 || (ARGV.count == 2 && (ARGV[1] == '-help' || ARGV[1] == '--help'))
        puts 'Usage: gdk init [DIR]'
        return false
      end

      directory = ARGV.count == 2 ? ARGV[1] : DEFAULT_INIT_DIRECTORY
      if directory.start_with?('-')
        warn <<~INVALID_GDK_DIR_NAME
          The gdk directory cannot start with a dash ('-').  Did you mean:
          gdk init #{directory.sub(/^-+/, '')}
        INVALID_GDK_DIR_NAME
        return false
      end

      cmd = %W[git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git #{directory}]
      system(*cmd) && remember!(directory)
    else
      if print_version?
        puts VERSION

        return true
      end

      unless gdk_dir?
        warn_not_gdk_dir

        return false
      end

      load(File.join(gdk_root, 'lib/gdk.rb'))
      GDK.main
    end
  end

  def self.warn_not_gdk_dir
    warn <<~NOT_A_GDK_DIR

      The current working directory is not inside a gitlab-development-kit
      installation. Use 'cd' to go to your gitlab-development-kit or create
      a new one with 'gdk init'.

      gdk init [DIRECTORY] # Default: #{DEFAULT_INIT_DIRECTORY}

    NOT_A_GDK_DIR
  end

  def self.gdk_root
    @gdk_root ||= find_root(Dir.pwd)
  end

  def self.gdk_dir?
    !gdk_root.nil?
  end

  def self.print_version?
    %w[version --version].include?(ARGV.first) && !gdk_dir?
  end

  def self.find_root(current)
    if File.exist?(File.join(current, 'GDK_ROOT'))
      File.realpath(current)
    elsif File.realpath(current) == '/'
      nil
    else
      find_root(File.join(current, '..'))
    end
  end

  def self.load_dotfile
    File.open(DOTFILE, File::RDONLY | File::CREAT) do |f|
      YAML.safe_load(f)
    end || {}
  end

  def self.remember!(directory)
    File.open("#{directory}/#{ROOT_CHECK_FILE}", 'w') do |f|
      f.puts File.realpath(directory)
    end
    true
  rescue StandardError => e
    warn e
    false
  end
end

exit(GDK.launcher_main)
